//requiere: function css (Ver post css dinamico capitulo 1)
var blinkingRectangle = (function() {
	function css_initial(parentSelector, divSelector, divCss) {
		var p = {};
		p[parentSelector] = {};
		p[parentSelector].position = 'relative';
		p[divSelector] = {};
		p[divSelector]['z-index'] = 99;
		p[divSelector].position = 'absolute';
		for (var prop in divCss) {
			p[divSelector][prop] = divCss[prop];
		}
		css(p);
	}
	return function(settings) {
		var el = document.querySelector(settings.selector);
		var div = document.createElement('div');
		div.dataset.blinking = '';
		var elChilds = [].slice.call(el.childNodes);
		//if (elChilds >= 1) {
			//el.insertBefore(div, elChilds[0]);
		//} else {
			el.appendChild(div);
		//}
		var divSelector = settings.selector + ' > div[data-blinking]';
		css_initial(settings.selector, divSelector,settings.css);
		var cssParam = {};
		cssParam[divSelector] = {};
		cssParam[divSelector].background = settings.initialValue;
		var toggle = function (val) {
			setTimeout(function() {
				cssParam[divSelector].background = val;
				css(cssParam);
				toggle(
					(settings.initialValue == val) ?
					settings.toggleValue : settings.initialValue
				);
			}, settings.duration);
		};
		toggle(settings.initialValue);
		return {
			stop:function(){
				toggle = function(){};
			}
		};
	};
})();

module.export = blinkingRectangle;