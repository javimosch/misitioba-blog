var blinkingRectangle = import('blinking');

var blinking = blinkingRectangle({
	selector: '.header .title a',
	duration: 3000,
	css: {
		width: '8.75vw',
		height: '1.2vw',
		left: '5.2vw',
		top: '13.7vw'
	},
	initialValue: 'transparent',
	toggleValue: 'white'
});


var css = (function() {
	var el = document.createElement('style');
	el.setAttribute('type', 'text/css');
	el.id = 'dynamicCss';
	document.querySelector('head').appendChild(el);
	var data = {};

	function render() {
		var html = "";
		for (var selector in data) {
			var rules = data[selector];
			html += selector + "{";
			for (var rule in rules) {
				html += rule + ": " + rules[rule] + ';';
			}
			html += "}";
		}
		el.innerHTML = html;
	}
	return function(selectors) { //rule = {body:{background:"red"}}
		for (var selector in selectors) {
			if(data[selector] === undefined){
				data[selector] = selectors[selector];	
			}else{
				for(var prop in selectors[selector]){
					data[selector][prop] = selectors[selector][prop];
				}
			}
		}
		render();
	};
})();

function randomColor(eachMilli) {
	setInterval(function() {
		css({
			body: {
				background: "#" + ((1 << 24) * Math.random() | 0).toString(16)
			}
		});
	}, eachMilli);
}

//randomColor(5000);


(function() {
	var rockClimbingTarget = document.querySelector('#rockClimbingTarget');

	function resizeEscalador() {
		if(!rockClimbingTarget) {
			return;
		}
		css({
			'div.rockclimbing img': {
				'max-height': rockClimbingTarget.offsetHeight + 'px'
			}
		});
	}
	window.onresize = resizeEscalador;
	resizeEscalador();
})();