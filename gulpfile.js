var gulp = require('gulp'),
  stylus = require('gulp-stylus'),
  connect = require('gulp-connect'),
  koutoSwiss = require("kouto-swiss"),
  jade = require('gulp-jade'),
  watch = require('gulp-watch'),
  YAML = require('yamljs');
  var browserify = require('gulp-browserify');

  var poststylus = require('poststylus');

var path = './themes/mba/';
var output = './dist';
var jadeInput = path + 'layout/test.jade';
var jadeWatch = path + 'layout/**/*.jade';
var jsInput = path + 'source/**/*.js';
var jsOutput = output + '/js';
var cssOutput = output + '/css';
var stylInput = path + 'source/css/style.styl';
var stylWatch = path + 'source/**/*';
var imgInput = path + 'source/img/*';
var imgOutput = output + '/img';

var hexoconfig = '_config.yml';
var hexotheme = path + '_config.yml';

console.info('Stylus watch '+stylWatch);

gulp.task('stylus', function() {
  watch(stylWatch, function() {
    gulp.src(stylInput)
      .pipe(
        stylus({
          use: [koutoSwiss()]
        })
      )
      .pipe(gulp.dest(cssOutput))
      .pipe(connect.reload());
    img(false);
  });
});

gulp.task('hexocss', function() {
  watch(stylWatch, function() {
    gulp.src(stylInput)
      .pipe(
        stylus({
          use: [koutoSwiss(),poststylus('lost')]
        })
      )
      .pipe(gulp.dest("public/css"))
      .pipe(connect.reload());
  });
});


function img(reload){
  reload = reload || true;
  console.log('copiando imagenes ('+reload+')');
  if(reload){
    gulp.src(imgInput).pipe(gulp.dest(imgOutput)).pipe(connect.reload());
  }
  else{
    gulp.src(imgInput).pipe(gulp.dest(imgOutput));
  }
}

gulp.task('jade', function() {
  watch(jadeWatch, function() {

    YAML.load(hexoconfig, function(result) {
      YAML.load(hexoconfig, function(theme) {
        var jadeLocals = {
          config: result,
          theme: theme
        };
        gulp.src(jadeInput)
          .pipe(jade({
            locals: jadeLocals,
            pretty: true
          }))
          .pipe(gulp.dest(output))
          .pipe(connect.reload());
      });
    });
  });


});

gulp.task('hexojs', function() {
    watch('_src/**/*.js', function() {
    gulp.src('_src/js/main.js')
        .pipe(browserify({
          insertGlobals : true,
          debug : true
        }))
        .pipe(gulp.dest('./public/js'));
    });
});

gulp.task('js', function() {
  watch(jsInput, function() {
    gulp.src(jsInput)
      .pipe(gulp.dest(jsOutput))
      .pipe(connect.reload());
  });
});

gulp.task('img', function() {
  watch(imgInput, function() {
    img();
  });
});


gulp.task('watch', function() {
  gulp.start('jade', 'stylus', 'js', 'img');
});

gulp.task('connect', function() {
  connect.server({
    root: './dist',
    port:2727,
    livereload: true
  });
});



gulp.task('mba', ['connect', 'watch']);

gulp.task('default', ['mba']);