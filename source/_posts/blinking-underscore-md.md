title: Como es que parpadea el guion (How to blinking underscore)
lang: es
date: 2015-12-03 03:09:28
tags:
---
### Prototipando y jugando con la dev console

Queria comentarles como logre el efecto de 'blinking' del 'underscore' del header. En spanish (perdon, no tengo enie), seria guion bajo, y me refiero al parpadeo que hace.

Empezamos jugando con la dev console y nos damos cuenta que el logo esta compuesto por un icono de fontawesome.

``` html
<i class="fa fa-terminal">MisitioBA</i>
```

Como ya sabemos, o por si no sabian, los iconos de fontawesome no son otra cosa mas que fuentes por lo que no cuentan con un ancho o un alto, sino que son afectados directamente por un attributo font-size, por ejemplo.

Entonces, dada esta investigacion inicial, podemos empezar a prototipar la solucion.
Devuelta en la dev console dibujamos un div por encima del tag 'i' que observamos arriba. A este div, lo posicionamos de forma absoluta con unidades de medida 'vw' (viewport width), que son afectadas con el resize de la pantalla, y de esta forma logramos un comportamiento identico al del icono. Tambien le agregamos un background de color llamativo para saber donde se dibuja.

{% asset_img underscore.post.1.jpg [title] %}

Cuando logramos tapar el guion con el div, simplemente cambiamos el color a blanco y vemos que funciona, el guion se tapa completamente por el div superpuesto. 
Pero si intentamos hacer resize a la pantalla vemos que se corre. La solucion es agregando position 'relative' al parent del div., como figura en la imagen, de esta manera el div se posicion de manera absoluta siempre respecto del parent.Con este ultimo cambio, tambien hace falta agregar 'z-index' al div para ponerlo por encima del icono.


#### Ahora, a por algo de javascript

Pensando un poco, todo esto del blinking underscore deberia ser una especie de componente, ya que como sabemos que tiene que parpadear una y otra ves, tambien va a tener javascript. El camino que seguimos es el de crear todo dinamicamente por javascript, incluido el div superpuesto. Eso si, antes lo prototipamos con la consola y llegamos a un resultado como este.

``` css
    div{
        background: white;
        width: 8.75vw;
        height: 1.2vw;
        position: absolute;
        left: 5.2vw;
        top: 11.6vw;
        z-index: 99;
    }
```

A continuacion vamos a generar el div anterior dinamicamente y hacer que parpadee, si todavia no lo hicieron, les recomiendo luego leer el siguiente post, dado que el css dinamico lo generamos utilizando ese snippet y tendran que incluirlo por su cuenta.

{% post_link css-dinamico-capitulo1 css dinamico capitulo 1: el snippet %}

El codigo final y que vamos a examinar es el siguiente:
``` javascript
//requiere: function css (Ver post css dinamico capitulo 1)
var blinkingRectangle = (function() {
	function css_initial(parentSelector, divSelector, divCss) {
		var p = {};
		p[parentSelector] = {};
		p[parentSelector].position = 'relative';
		p[divSelector] = {};
		p[divSelector]['z-index'] = 99;
		p[divSelector].position = 'absolute';
		for (var prop in divCss) {
			p[divSelector][prop] = divCss[prop];
		}
		css(p);
	}
	return function(settings) {
		var el = document.querySelector(settings.selector);
		var div = document.createElement('div');
		div.dataset.blinking = '';
		el.appendChild(div);
		var divSelector = settings.selector + ' > div[data-blinking]';
		css_initial(settings.selector, divSelector,settings.css);
		var cssParam = {};
		cssParam[divSelector] = {};
		cssParam[divSelector].background = settings.initialValue;
		var toggle = function (val) {
			setTimeout(function() {
				cssParam[divSelector].background = val;
				css(cssParam);
				toggle(
					(settings.initialValue == val) ?
					settings.toggleValue : settings.initialValue
				);
			}, settings.duration);
		};
		toggle(settings.initialValue);
		return {
			stop:function(){
				toggle = function(){};
			}
		};
	};
})();

var blinking = blinkingRectangle({
	selector: '.header .title a',
	duration: 3000,
	css: {
		width: '8.75vw',
		height: '1.2vw',
		left: '5.2vw',
		top: '13.7vw'
	},
	initialValue: 'transparent',
	toggleValue: 'white'
});
```


#### Examinemos la llamada:

Como **'selector'**, le pasamos el selector del parent que mencionabamos a principio del post. Es el elemento que contiene el div que se superpone, al que me referire como
**blinkingElement**.
- duration (number) : duracion entre cambio de color del background.
- css (object) : css por defecto del **blinkingElement**
- initialValue (hexa,string,rgb,rgba): valor inicial de color para el background de **blinkingElement**
- toggleValue (hexa,string,rgb,rgba): valor con el que ira haciendo el 'toggle'.



'Toggle' significa alternar.
``` javascript
var blinking = blinkingRectangle({
    selector: '.header .title a',
    duration: 3000,
    css: {
        width: '8.75vw',
        height: '1.2vw',
        left: '5.2vw',
        top: '13.7vw'
    },
    initialValue: 'transparent',
    toggleValue: 'white'
});
```


Entonces, veamos como se compone la definicion de  **blinkingRecangle**.

Generalmente, cuando creamos un componente, que se utiliza como funcion, el esqueleto que suelo utilizar es el siguiente:

``` javascript
var componenteComoFuncion = (function(){
    //scope privado del componente
    return function(){
        //scope de la funcion que se utiliza.
    }
})();
```

Hay un monton para hablar sobre esta forma de codear, asi que en este post solo voy a decir dos cosas. Uno, no estoy loco. Dos, mas tarde voy a crear un post para hablar en detalle de esto. Y demas esta decir que vale la pena enterarse.

Continuamos. Dicho lo anterior repasemos el 'scope de la funcion que se utiliza'.

#### 'scope de la funcion que se utiliza'

``` javscript
    return function(settings) {
       
        //guardamos el parent de blinkingElement
		var el = document.querySelector(settings.selector);
	
		//creamos el tag para blinkingElement
		var div = document.createElement('div');
	
		//le seteamos un dataset para poder encontrarlo con selectores css
		div.dataset.blinking = '';
		
		//lo agregamos al dom
		el.appendChild(div);
	
		//creamos el selector para acceder al blinkingElement
		var divSelector = settings.selector + ' > div[data-blinking]';
	
		//seteamos los valores de css por defecto
		//(mas abajo veremos la definicion de esta funcion)
		css_initial(settings.selector, divSelector,settings.css);
	
		//creamos el parametro para asignar el css dinamico
		var cssParam = {};
		cssParam[divSelector] = {};
		cssParam[divSelector].background = settings.initialValue;
		
		//creamos la funcion toggle, para alternar el valor
		var toggle = function (val) {
			setTimeout(function() {
			    
			    //asigna el nuevo valor al parametro de css dinamico
				cssParam[divSelector].background = val;
			
				//realizamos el cambio en el dom
				css(cssParam);
				
				//se vuelve a llamar asi mismo con el valor opuesto
				toggle(
					(settings.initialValue == val) ?
					settings.toggleValue : settings.initialValue
				);
				
				//utilizamos el duration que pasamos como parametro 
			}, settings.duration);
		};
		
		//llamada inicial 
		toggle(settings.initialValue);
		return {
		    
		    //como utilidad extra, devolvemos un metodo
		    //para detener el comportamiento
			stop:function(){
			    
			    //simplemente hace un override para que no se siga llamando
				toggle = function(){};
			}
		};
	};
```
#### 'scope privado del componente'


``` javascript
	function css_initial(parentSelector, divSelector, divCss) {
	    
	    //crea el parametro para el css-dinamico
		var p = {};
		p[parentSelector] = {};
		
		//como dijimos al principio, parent posicion relativo
		p[parentSelector].position = 'relative';
		p[divSelector] = {};
		
		//valores por defecto para el blinkingElement
		p[divSelector]['z-index'] = 99;
		p[divSelector].position = 'absolute'
	
		//mas lo que pasamos como parametro a la funcion;
		//(las medidas, width, heigth, top, etc)
		for (var prop in divCss) {
			p[divSelector][prop] = divCss[prop];
		}
		
		//aplicamos los cambios
		css(p);
	}
```

Eso seria todo, este mismo codigo es que lo que hace que el guion de arriba parpadee.
Mas que nada queria explicarlo por que me imagino un escenario en el que viene mi novia y me dice 'ah mira que simpatico, parpadeea', y no se da una idea que 'under the skin' hay un poco de codigo. No parpardea por arte de magia.


Abrazo gente, voy a ver si pongo el sistema de comentarios de 'Disqus' asi me pueden empezar a bardear los posts.



