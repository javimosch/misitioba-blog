title: 'CSS Dinamico Capitulo 1: El snippet'
lang: en
tags:
  - ''
  - css
  - javascript
categories:
  - programming
date: 2015-11-30 02:31:00
---

Seguro muchas veces estuviste en una situacion en la que tenias que insertar css de forma dinamica a tu sitio y no sabias por donde empezar.
La cuestion es que es mas facil de lo que parece.
A continuacion les muestro un snippet que realice hace un minuto. De enserio, fue hace un minuto.


Si no entienden mucho el codigo no se preocupen, vamos a por ello.
Lo primero que hacemos es hacernos con un elemento del DOM dinamico, creado por nosotros, en este caso, seria un tag style, y lo dejamos a disposicion.
```javascript
	var el = document.createElement('style');
	el.setAttribute('type', 'text/css');
	el.id = 'dynamicCss';
	document.querySelector('head').appendChild(el);
```

Ahora sabemos que a 'el', que representa el tag style que ya se encuentra renderizado por el browser, le podemos dar masa con lo que queramos, en este caso, nuestro preciado css dinamico.

Queremos insertar css dinamico de una forma comoda, y practica, y no perder el css que agregamos cada ves que agregamos mas. Una forma practica que se me ocurrio es pasarlo como un objeto de javascript.
```javascript
	var codigoCSS = {body:{background:'red'}};
```
Se ve bastante legible verdad?. Y si formateamos el codigo mejor. Entonces como logramos esto?. Pues con una funcion a disposicion como la siguiente:
```javascript
	var data = {};
	return function(selectors) {
	    for (var selector in selectors) {
            data[selector] = selectors[selector];
        }
        render();
    };
```
Es tan sencillo como ir almacenando todos los datos que van llegando en un objeto 'data'.
Por ultimo, una funcion que se encargue de leer este objeto data y armar el codigo css que corresponde, para luego realizar un simple innerHTML al tag css dinamico que generamos anteriormente y que tenemos en la mano.

```javascript
	 function render() {
        var html = "";
        for (var selector in data) {
            var rules = data[selector];
            html += selector + "{";
            for (var rule in rules) {
                html += rule + ": " + rules[rule];
            }
            html += "}";
        }
        el.innerHTML = html;
    }
```

Y ahora, el codigo final


```javascript
var css = (function() {
	var el = document.createElement('style');
	el.setAttribute('type', 'text/css');
	el.id = 'dynamicCss';
	document.querySelector('head').appendChild(el);
	var data = {};

	function render() {
		var html = "";
		for (var selector in data) {
			var rules = data[selector];
			html += selector + "{";
			for (var rule in rules) {
				html += rule + ": " + rules[rule];
			}
			html += "}";
		}
		el.innerHTML = html;
	}
	return function(selectors) { //rule = {body:{background:"red"}}
		for (var selector in selectors) {
			data[selector] = selectors[selector];
		}
		render();
	};
})();
```

Espero que les sirva, abrazos!.
