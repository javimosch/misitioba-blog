var css = (function() {
	var el = document.createElement('style');
	el.setAttribute('type', 'text/css');
	el.id = 'dynamicCss';
	document.querySelector('head').appendChild(el);
	var data = {};

	function render() {
		var html = "";
		for (var selector in data) {
			var rules = data[selector];
			html += selector + "{";
			for (var rule in rules) {
				html += rule + ": " + rules[rule] + ';';
			}
			html += "}";
		}
		el.innerHTML = html;
	}
	var fn= function(selectors) { //rule = {body:{background:"red"}}
		for (var selector in selectors) {
			if(data[selector] === undefined){
				data[selector] = selectors[selector];	
			}else{
				for(var prop in selectors[selector]){
					data[selector][prop] = selectors[selector][prop];
				}
			}
		}
		render();
	};
	fn.to=function(selector){
		var p = {};
		p[selector] = {};
		return {
			set:function(props){
				p[selector] = props;
				fn(p);
			}
		};
	};
	return fn;
})();
module.exports = css;
top.css = css;