var blinkingRectangle = require('./blinking');

module.exports = function() {
	var blinking = blinkingRectangle({
		selector: "i.fa-terminal",
		duration: 3000,
		css: {
			width: '3.10vw',
			height: '0.6vw',
			left: '1.7vw',
			top: '3.7vw'
		},
		initialValue: 'transparent',
		toggleValue: 'white'
	});

	//width: 3.10vw;
    //height: 0.6vw;
    //left: 1.7vw;
    //top: 6.7vw;

	//blinking.stop();
};