var easing = (function() {


	var fn = function(s, cb,endCb) {
		var start = new Date().getTime();
		var end = start + s.duration;

		function loop() {
			setTimeout(function() {
				var now = new Date().getTime();
				var elapsed = now - start;
				s.val = s.easingFunction(elapsed, s.from, s.to, s.duration);
				if (now >= end) {
					cb(s.to);
					endCb();
					return;
				} else {
					cb(s.val);
					loop();
				}
			}, s.interval);
		}
		loop(s.from);
	};
	fn.linearTween = function(t, b, c, d) {
		return c * (t / d) + b;
	};
	fn.easeInQuad = function(t, b, c, d) {
		t /= d;
		return c * t * t + b;
	};
	return fn;
})();
module.exports = easing;
/*
easing({
	easingFunction: easing.easeInQuad,
	from: 0,
	to: 10,
	duration: 5000,
	interval: 500
}, function(val) {
	console.info(val);
});
*/