var bg  = require('./anims.bg');
var blinkingUnderscode  = require('./anims.blinking.underscore');

module.exports = function(){
	bg();
	blinkingUnderscode();
};