var bgcool = require('./bgcool.js');
var easing = require('./easing.js');

var bg =null;
function init() {
	bg = bgcool({
		selector: '.main-content.bg2',
		gradients: ['rgba(255,255,255,0.6)', 'rgba(255,0,255,0.35)', 'rgba(255,0,0,0.35)'],
		assets: '../img/',
		images: [
		'background1.jpg', 
		'background2.jpg', 
		'background3.jpg',
		'bg1.jpg',
		'bg2.jpg',
		'bg3.jpg',
		'bg6.jpg',
		'bg7.jpg',
		]
	});
}

var moveX = false;
var indexAnt = null;
function rnd(){
	return Math.floor(Math.random() * (bg.length - 1 - 0 + 1)) + 0;
}
function anim() {
	var index = 0;
	do{
		index = Math.floor(Math.random() * (bg.length - 1 - 0 + 1)) + 0;
	}while(index==indexAnt);
	indexAnt = index;

	bg.set(index);
	moveX = Math.floor(Math.random() * (1000 - 1 - 0 + 1)) + 0 < 500;
	//
	easing({
		easingFunction: easing.easeInQuad,
		from: 0,
		to: -2000,
		duration: 5000,
		interval: 500
	}, function(val) {

		if (moveX) {
			bg.pos(val, 0);
		} else {
			bg.pos(0, val);
		}
	}, function() {
		anim();
	});
}

module.exports = function(){
	init();
	anim();
};