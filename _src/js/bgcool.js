var bgcool = (function() {
	var cssInit = function(image, gradients) {
		return {
			'background-repeat': 'repeat',
			'background-size': 'cover',
			'min-height': '600px',
			'background': 'linear-gradient(' + cssGradients(gradients) + '), url(' + image + ')'
		};
	};
	var cssGradients = function(gradients) {
		var rta = '';
		gradients.forEach(function(v) {
			rta += v + ', ';
		});
		return rta.substring(0, rta.length - 2);
	};
	var cssPos = function(x,y){
		return {
			'background-position-x': x+'px',
			'background-position-y': y+'px'
		};
	};
	var cssChangeImage = function(image, gradients) {
		return {
			'background': 'linear-gradient(' + cssGradients(gradients) + '), url(' + image + ')'
		};
	};
	return function(settings) {
		var i = settings.assets+settings.images[0];
		css.to(settings.selector).set(cssInit(i, settings.gradients));
		console.log('bgSlide.init');
		return {
			images:settings.images,
			length:settings.images.length,
			pos:function(x,y){
				this.x = x || 0; this.y = y || 0;
				css.to(settings.selector).set(cssPos(this.x,this.y));
			},
			set:function(index){
				var i = settings.assets+settings.images[index];
				css.to(settings.selector).set(cssChangeImage(i,settings.gradients));
			}
		};
	};
})();
module.exports = bgcool;
/*
bgcool/*({
	selector: '.main-content.bg2',
	gradients: ['rgba(255,255,255,0.6)', 'rgba(255,0,255,0.35)'],
	assets: '../img/',
	images: ['background1.jpg', 'background2.jpg', 'background3.jpg']
});
*/